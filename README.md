# forza4 for the Z88

This is an adaption of the following code

https://www.z88dk.org/wiki/doku.php?id=examples:snippets:forza4

and

http://www.pierotofy.it/pages/sorgenti/dettagli/19253-Forza_4/

## Visuals

https://gitlab.com/JB1zzel1/forza4-for-the-z88/-/raw/main/Capture1.PNG

## Installation

Compile with Z88DK using a command like this...

`zcc +z88 -clib=ansi Con4.c`

Or, use the C4.bbc file,

load it into your z88, makesure it's in RAM, then load Basic and type `RUN"C4.BBC"` and press ENTER


## Roadmap

This is very much a WIP - but it does work and you can play angainst the CPU or other human.

## Contributing
I am open to contributions 

## Authors and acknowledgment

Original program:  http://www.pierotofy.it/pages/sorgenti/dettagli/19253-Forza_4/

## License


## Project status
WIP / in development
